from pkg_resources import resource_listdir
from fastapi import FastAPI
import psycopg 
from pydantic import BaseModel

app = FastAPI()

@app.get("/hello-world")
def hello_world():
  return {"message":"Hello"}

@app.get("/")
async def root():
    return {"message": "Howdy"}


@app.get("/api/categories")
def list_categories(page: int = 0): #what is page signifying 
  conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"  #how do i know where to look for this data base 
  with psycopg.connect(conn_str) as conn: # inherits the conn_str variable which is a link to the database 
      with conn.cursor() as cur: # opens a cursor to perform database operations - what is a cursor ? 
          cur.execute("""
              SELECT id, title, canon
              FROM categories
              LIMIT 100
              OFFSET %s
          """, [page * 100])
          categories = cur.fetchall() # fetch all returns all the records 
          results = []
          for record in categories:
              result = {
                "id": record[0],
                "title": record[1],
                "canon": record[2]
              }
              results.append(result)
          return results 

@app.get("/api/categories/{category_id}")
def get_category(category_id: int): # as in /1 /2 /3 ? 
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT id, title, canon
                FROM categories
                WHERE id = %s 
            """, [category_id])  #?? what is the % s doing 
            record = cur.fetchone()
            return {
                "id": record[0],
                "title": record[1],
                "canon": record[2]
            }


class Category(BaseModel):
    id: int
    title: str
    canon: bool

@app.post("/api/categories")
def create_category(category: Category):
    print(category)
    return "YAY!" 

@app.put(
    "/api/categories/{category_id}",
    response_model=Category,
    responses={404: {"model": Message}},
)
def update_category(category_id: int, category: CategoryIn, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE categories
                SET title = %s
                WHERE id = %s;
            """,
                [category.title, category_id],
            )
    return get_category(category_id, response)


@app.delete(
    "/api/categories/{category_id}",
    response_model=Message,
    responses={400: {"model": Message}},
)
def remove_category(category_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                    """
                    DELETE FROM categories
                    WHERE id = %s;
                """,
                    [category_id],
                )
                return {
                    "message": "Success",
                }
            except psycopg.errors.ForeignKeyViolation:
                response.status_code = status.HTTP_400_BAD_REQUEST
                return {
                    "message": "Cannot delete category because it has clues",
                }
        